# README #

In this project I included the required cli + a few tests + simple web API server.

### How do I get set up? ###

* pip install -r requirements.txt
* You can use the cli with `python cli.py <N>`
* You can run the server with `python server.py` - then use the server with 
  `curl localhost:5000/trending-repos/<n>`
  
I assumed that I should return at most the number of results on github trending repositories web page, even if `N`
is larger.

Also, I concentrated on making it work,
so it happens it takes a long time to get the response.

I would also consider the long time to get a response in the API if I had more time

