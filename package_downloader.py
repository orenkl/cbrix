import os
import shutil
from pathlib import Path

import git


class PackageDownloader:
    DEFAULT_DIR_PATH =  'downloaded_packages'
    def __init__(self, dir_path=None):
        dir_path = dir_path or self.DEFAULT_DIR_PATH
        if not Path(dir_path).is_dir():
            os.mkdir(dir_path)
        self.dir_path = dir_path

    def download(self, package_name, package_url):
        destination_path = Path(self.dir_path) / package_name
        if not destination_path.exists():
            git.repo.base.Repo.clone_from(package_url, destination_path.as_posix())
        # subprocess.check_call([sys.executable, "-m", "pip", "download", "--no-deps", "-d", self.dir_path, package])

    def clean_up(self, package_name=None):
        '''
        Clean up a package_name. if no package name was passed, cleans up all of the dowanloads directory
        :param package_name:
        '''
        if package_name:
            shutil.rmtree(f"{self.dir_path}/{package_name}")
        else:
            shutil.rmtree(f"{self.dir_path}")

    def get_downloaded_package_path(self, package_name):
        packages_paths = os.listdir(self.dir_path)
        for package_path in packages_paths:
            if package_path == package_name:
                return package_path
        raise FileExistsError('package_name was not downloaded')
