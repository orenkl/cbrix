import subprocess

from package_downloader import PackageDownloader


class PackageSecurityChecker:
    def __init__(self, downloaded_packages_path=None):
        self.package_downloader = PackageDownloader(downloaded_packages_path)

    def  calc_risk_score(self, package_name, package_url):
        self.package_downloader.download(package_name, package_url)
        package_path = self.package_downloader.get_downloaded_package_path(package_name)
        result = subprocess.run(
            [
                "pip-extra-reqs",
                f"--requirements-file={self.package_downloader.dir_path}/{package_path}/requirements.txt",
                f"{self.package_downloader.dir_path}/{package_path}"
            ],
            text=True,
            capture_output=True
        )
        self.package_downloader.clean_up(package_name)
        return len(result.stderr.splitlines())
