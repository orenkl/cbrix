import argparse
import sys

import gtrending

from package_security_checker import PackageSecurityChecker


class GitHubCLI:
    def __init__(self, args):
        parser = argparse.ArgumentParser(description='Process some integers.')
        parser.add_argument('top_n_repos', metavar='N', type=int, help='top n repositories to get from GitHub')
        self.args = parser.parse_args(args)

        self.risk_calculator = PackageSecurityChecker()


    def run(self, should_print=True):
        repos = gtrending.fetch_repos(language='python')
        results = []
        filtered_repos = repos[:self.args.top_n_repos] if self.args.top_n_repos else repos
        for repo_data in filtered_repos:
            risk_score = self.calc_risk_score(repo_data)
            result = dict(repo_data)
            result['risk_score'] = risk_score
            results.append(result)
        if should_print:
            print('\n'.join(map(str, results)))
        return results

    def calc_risk_score(self, repo_data):
        return self.risk_calculator.calc_risk_score(repo_data['name'], repo_data['url'])



if __name__ == "__main__":
    cli = GitHubCLI(sys.argv[1:])
    results = cli.run()
