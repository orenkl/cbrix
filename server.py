from flask import Flask, jsonify

from cli import GitHubCLI

app = Flask(__name__)


@app.route("/trending-repos/<top_n>")
def main(top_n):
    results = GitHubCLI(top_n).run(should_print=False)
    return jsonify(results)


if __name__ == '__main__':
    app.run()
