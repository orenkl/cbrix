from pathlib import Path
from unittest import TestCase

from package_downloader import PackageDownloader
from package_security_checker import PackageSecurityChecker


class TestPackageSecurityChecker(TestCase):
    def setUp(self) -> None:
        self.security_checker = PackageSecurityChecker(
            (Path(__file__).parent / 'test_security_checker_downloaded_packages').as_posix()
        )

    def tearDown(self) -> None:
        self.security_checker.package_downloader.clean_up()

    def test_download_package(self):
        package_name = 'paddlex'
        package_url = 'https://github.com/PaddlePaddle/PaddleX'
        calculated_risk = self.security_checker.calc_risk_score(package_name, package_url)
        self.assertEqual(calculated_risk, 15)