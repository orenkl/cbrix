from pathlib import Path
from unittest import TestCase

from package_downloader import PackageDownloader


class TestPackageDownloader(TestCase):
    def setUp(self) -> None:
        self.package_downloader = PackageDownloader()

    def tearDown(self) -> None:
        self.package_downloader.clean_up()

    def test_download_package(self):
        package_name = 'paddlex'
        package_url = 'https://github.com/PaddlePaddle/PaddleX'
        self.package_downloader.download(package_name, package_url)
        downloaded_package_path = self.package_downloader.get_downloaded_package_path(package_name)
        self.assertTrue(downloaded_package_path.startswith(package_name))